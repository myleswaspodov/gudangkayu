<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers;

use App\Http\Models\User;
use App\Http\Models\Slot;
use App\Http\Models\SlotKayu;
use App\Http\Models\LogIn;
use Hash;
use Auth;

use App\Http\Requests\Users\Create;
use App\Http\Requests\Users\ChangePassword;

class InController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        date_default_timezone_set('Asia/Jakarta');
    }

    /* CREATE */
    function create(Request $request) {
        $post = $request->except('_token');

        if (empty($post)) {
            $data = [
                'title'    => 'Transaksi Masuk',
                'menu'     => 'in',
                'sub_menu' => 'in tambah'
            ];

            $data['slot'] = Slot::get()->toArray();

            return view('content.in.create', $data);
        }
        else {
            // SAVE TRANSAKSI
            $save = LogIn::saveTrx($post);

            return parent::redirect($save, 'Data transaksi masuk berhasil ditambahkan.');
        }
    }

    /* LIST */
    function index(Request $request) {
        $post = $request->except('_token');

        $data = [
            'title'      => 'Transaksi Masuk List',
            'menu'       => 'in',
            'sub_menu'   => 'in list',
            'date_start' => date('Y-m-d', strtotime("- 7 days")),
            'date_end'   => date('Y-m-d'),
            'slot'       => Slot::get()->toArray()
        ];

        if (!empty($post)) {
            $post = array_filter($post);
            
            // ASSIGN DATA
            foreach ($post as $key => $value) {
                $data[$key] = $value;
            }
        }
        
        $data['transaksi'] = LogIn::listLogIn($data);
        
        return view('content.in.list', $data);
    }
}