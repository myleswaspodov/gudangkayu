<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class FeatureControl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $feature)
    {
      $user = Session::get('user');
      $user = $user['level'];

      if (stristr($feature, "|")) {
        $feature = explode("|", $feature);
      }

      if (is_array($feature)) {
        if (in_array($user, $feature)) {
          return $next($request);
        }
        else {
          $feature = implode(",", $feature);
          
          return redirect('home')->withErrors(['Fitur ini hanya bisa diakses oleh ('.$feature.'). Silakan hubungi administrator.']);
        }
      }
      else {
        if ($user == $feature) {
          return $next($request);
        }
        else {
          return redirect('home')->withErrors(['Fitur ini hanya bisa diakses oleh ('.$feature.'). Silakan hubungi administrator.']);
        }
      }
      
    }
}
