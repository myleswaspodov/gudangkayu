<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

use App\Http\Models\Slot;
use App\Http\Models\Kayu;
use App\Http\Models\SlotKayu;

use DB;

/**
 * @property int $id_log_in
 * @property int $id_slot_kayu
 * @property int $qty
 * @property string $created_at
 * @property string $updated_at
 * @property SlotKayu $slotKayu
 */
class LogIn extends Model
{
    public function __construct()
    {
        date_default_timezone_set('Asia/Jakarta');
    }
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'log_in';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_log_in';

    /**
     * @var array
     */
    protected $fillable = ['id_slot_kayu', 'qty', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function slotKayu()
    {
        return $this->belongsTo('App\Http\Models\SlotKayu', 'id_slot_kayu', 'id_slot_kayu');
    }

    /* SAVE TRX */
    static function saveTrx($post) {
        DB::beginTransaction();
        // CEK TRX
        $cekTrx = Slot::where('id_slot', $post['id_slot'])->first();

        if (empty($cekTrx)) {
            return false;
        }
        else {
            $jumlahSlot = $cekTrx->capacity;

            // CEK SLOT YANG MASIH ADA DI SLOT KAYU
            $cekTrxSlot = SlotKayu::where('id_slot', $post['id_slot'])->where('flag', '=', '0')->sum('qty');

            if ($jumlahSlot > $cekTrxSlot) {
                // SAVE KAYU
                $saveKayu = Kayu::create([
                    'length' => $post['length'],
                    'width'  => $post['width'],
                    'code'   => time()
                ]);

                if ($saveKayu) {
                    // SAVE SLOT KAYU
                    $saveSlotKayu = SlotKayu::create([
                        'id_slot' => $post['id_slot'],
                        'id_kayu' => $saveKayu->id_kayu,
                        'qty'     => $post['qty'],
                        // FLAG 0 UNTUK PENANDA SLOT MASUK
                        // FLAG 1 UNTUK PENANDA SLOT KELUAR
                        'flag'    => 0
                    ]);

                    if ($saveSlotKayu) {
                        // CATAT LOG MASUK
                        $saveLogIn = Self::create([
                            'id_slot_kayu' => $saveSlotKayu->id_slot_kayu,
                            'qty'          => $post['qty']
                        ]);

                        if ($saveSlotKayu) {
                            DB::commit();

                            return ['Berhasil menyimpan data'];
                        }
                        else {
                            return false;
                        }
                    }
                    else {
                        DB::rollback();
                        return false;
                    }
                }
                else {
                    DB::rollback();
                    return false;
                }
            }
            else {
                DB::rollback();
                return false;
            }
        }
    }

    /* LIST */
    static function listLogIn($post) {
        $list = Self::leftJoin('slot_kayu', 'slot_kayu.id_slot_kayu', '=', 'log_in.id_slot_kayu')->select('log_in.*')->with(['slotKayu', 'slotKayu.kayu', 'slotKayu.slot']);

        if (isset($post['id_log_in'])) {
            $list->where('id_log_in', $post['id_log_in']);
        }

        if (isset($post['id_slot'])) {
            $list->where('id_slot', $post['id_slot']);
        }

        if (isset($post['date_start']) && isset($post['date_end'])) {
            $list->whereBetween('log_in.created_at', [$post['date_start'].' 00:00:00', $post['date_end'].' 23:59:59']);
        }

        $list = $list->get()->toArray();

        return $list;
    }
}
