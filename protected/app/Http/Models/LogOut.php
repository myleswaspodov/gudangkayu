<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_log_out
 * @property string $to
 * @property string $address
 * @property string $delivery_date
 * @property string $licence_plate
 * @property string $created_at
 * @property string $updated_at
 * @property LogOutDetail[] $logOutDetails
 */
class LogOut extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'log_out';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_log_out';

    /**
     * @var array
     */
    protected $fillable = ['to', 'address', 'delivery_date', 'licence_plate', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logOutDetails()
    {
        return $this->hasMany('App\Http\Models\LogOutDetail', 'id_log_out', 'id_log_out');
    }
}
