<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_log_out_detail
 * @property int $id_log_out
 * @property int $id_slot_kayu
 * @property int $qty
 * @property string $remark
 * @property string $created_at
 * @property string $updated_at
 * @property LogOut $logOut
 * @property SlotKayu $slotKayu
 */
class LogOutDetail extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'log_out_detail';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_log_out_detail';

    /**
     * @var array
     */
    protected $fillable = ['id_log_out', 'id_slot_kayu', 'qty', 'remark', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function logOut()
    {
        return $this->belongsTo('App\Http\Models\LogOut', 'id_log_out', 'id_log_out');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function slotKayu()
    {
        return $this->belongsTo('App\Http\Models\SlotKayu', 'id_slot_kayu', 'id_slot_kayu');
    }
}
