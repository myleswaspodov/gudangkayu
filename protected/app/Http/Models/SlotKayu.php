<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_slot_kayu
 * @property int $id_slot
 * @property int $id_kayu
 * @property int $qty
 * @property boolean $flag
 * @property string $created_at
 * @property string $updated_at
 * @property Kayu $kayu
 * @property Slot $slot
 * @property LogIn[] $logIns
 * @property LogOutDetail[] $logOutDetails
 */
class SlotKayu extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'slot_kayu';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_slot_kayu';

    /**
     * @var array
     */
    protected $fillable = ['id_slot', 'id_kayu', 'qty', 'flag', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kayu()
    {
        return $this->belongsTo('App\Http\Models\Kayu', 'id_kayu', 'id_kayu');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function slot()
    {
        return $this->belongsTo('App\Http\Models\Slot', 'id_slot', 'id_slot');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logIns()
    {
        return $this->hasMany('App\Http\Models\LogIn', 'id_slot_kayu', 'id_slot_kayu');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logOutDetails()
    {
        return $this->hasMany('App\Http\Models\LogOutDetail', 'id_slot_kayu', 'id_slot_kayu');
    }
}
