<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

/**
 * @property int $id_user
 * @property int $id_bidang
 * @property int $id_jabatan
 * @property string $name
 * @property string $email
 * @property string $npk
 * @property string $password
 * @property string $id_admin
 * @property string $remember_token
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property Jabatan $jabatan
 * @property Bidang $bidang
 * @property Disposisi[] $disposisis
 * @property Notif[] $notifs
 */
class User extends Authenticatable
{
    use Notifiable;
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id';

    protected $hidden = ['password'];

    // protected $table = 'users';

    /**
     * @var array
     */
    protected $fillable = ['nip', 'name', 'phone', 'email', 'password', 'level', 'remember_token', 'created_at', 'updated_at', 'deleted_at'];

    /* SAVE */
    static function saveData($data) {
        $data['password'] = Hash::make($data['nip']);
        $save             = Self::create($data);

        return $save;
    }

    /* LIST */
    static function listUser($data=[]) {
        $user = Self::select('*');

        if (isset($data['id'])) {
            $user->where('id', $data['id']);
        }

        if (isset($data['nip'])) {
            $user->where('nip', $data['nip']);
        }

        $user = $user->get()->toArray();

        return $user;
    }

    /* UPDATE */
    static function updateData($data) {
        $user = Self::where('id', $data['id'])->update($data);

        return $user;
    }
    
}
