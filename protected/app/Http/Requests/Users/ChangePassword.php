<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class ChangePassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password_now'          => 'sometimes|required',
            'password'              => 'sometimes|confirmed|required|min:3|max:50',
        ];
    }

    public function messages() {
        return [
            'password_now.required' => 'Password sekarang wajib diisi',
            'password.required'     => 'Password baru wajib diisi',
            'password.confirmed'    => 'Password konfirmasi tidak cocok',
        ];
    }
}