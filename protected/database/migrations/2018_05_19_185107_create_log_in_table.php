<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogInTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_in', function (Blueprint $table) {
            $table->increments('id_log_in');
            
            // slot kayu
            $table->unsignedInteger('id_slot_kayu');
            $table->foreign('id_slot_kayu')->references('id_slot_kayu')->on('slot_kayu')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->integer('qty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_in');
    }
}
