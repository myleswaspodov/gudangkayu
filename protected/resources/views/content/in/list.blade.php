@extends('template.body')

@section('style')
	
	<link rel="stylesheet" href="{{ url('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ url('bower_components/select2/dist/css/select2.min.css">') }}">
@endsection

@section('content')		 
<!-- Content Header (Page header) -->
<section class="content-header">
  	<div>
	    <ol class="breadcrumb">
	    	<li><i class="fa fa-home"></i> Home</li>
	    	<li>In</li>
	    	<li class="active">List</li>
	  	</ol>
	</div>
</section>

<!-- Main content -->
<section class="content">
	@include('template.alert')
	
	<div class="box">
	    <div class="box-header with-border">
	      <h3 class="box-title">{{ $title }}</h3>
	    </div>
	    <div class="box-body">
		    <form class="form-horizontal" action="{{ url()->current() }}" method="POST">
		      	<div class="box-body">
                    <div class="form-group">
                      	<label class="col-md-2 control-label">Jenis / Slot</label>
                      	<div class="col-md-6">
            	          	<select class="form-control" data-placeholder="Pilih Slot" name="id_slot">
            	          	<option value="">Semua Slot</option>
            	          	@if (!empty($slot))
    	                  		@foreach($slot as $val)
    	        					<option value="{{ $val['id_slot'] }}" @if(isset($id_slot)) @if ($id_slot == $val['id_slot']) selected @endif @endif> {{ $val['slot_name'] }}</option>
    	        				@endforeach
            				@endif
            				</select>
                      </div>
                    </div>
			        <div class="form-group">
			          	<label class="col-md-2 control-label">Rentang Tanggal </label>
			          	<div class="col-md-3">
			            	<input type="date" class="form-control" name="date_start" placeholder="Tanggal awal" value="{{ date('Y-m-d', strtotime($date_start)) }}">
			          	</div>
			          	<div class="col-md-3">
			            	<input type="date" class="form-control" name="date_end" placeholder="Tanggal akhir" value="{{ date('Y-m-d', strtotime($date_end)) }}">
			          	</div>
			        </div>
			        
		      	</div>
		      <!-- /.box-body -->
		    <div class="box-footer">
				<div class="col-md-2">		
				</div>
				<div class="col-md-10">
					<button type="reset" class="btn btn-default">Reset</button>
					<button type="submit" class="btn btn-info">Submit</button>
					{{ csrf_field() }}	
				</div>
		    </div>
		      <!-- /.box-footer -->
		    </form>
		    <hr>
		    <table id="table01" class="table table-bordered table-striped">
		        <thead>
		        <tr>
		          	<th>Tanggal Masuk</th>
		          	<th>Slot / Kayu</th>
		          	<th>Kuantitas</th>
		        </tr>
		        </thead>
		        <tbody>
		        @if (!empty($transaksi))
			        @foreach ($transaksi as $key=>$val)
			        	<tr>
				          	<td style="width: 300px;">{{ date('d F Y', strtotime($val['created_at'])) }}</td>
				          	<td>{{ $val['slot_kayu']['slot']['slot_name'] }}</td>
				          	<td style="width: 300px;">{{ $val['qty'] }}</td>
				        </tr>
				    @endforeach
			    @endif
		        </tbody>
		    </table>
	    </div>
	</div>

</section>
<!-- /.content -->


@endsection

@section('script')
<script src="{{ url('bower_components/bootstrap-confirmation/bootstrap-confirmation.min.js') }}"></script>
<script src="{{ url('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>
<script src="{{ url('js/bootstrap-confirmation.js') }}"></script>
<script>
  $(function () {
    $('#table01').DataTable();
    
  });
</script>
<script type="text/javascript">
	$('[data-toggle=confirmation-popout]').confirmation({
	    rootSelector: '[data-toggle=confirmation-popout]',
	    container: 'body'
	});
</script>
<script type="text/javascript">
	$('#table01').on('click', '.delete', function() {
		$('#delete-'+$(this).data('id')).submit();
	});

</script>

@endsection 