@extends('template.body')

@section('style')
	
	<link rel="stylesheet" href="{{ url('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ url('bower_components/select2/dist/css/select2.min.css">') }}">
@endsection

@section('content')		 
<!-- Content Header (Page header) -->
<section class="content-header">
  	<div>
	    <ol class="breadcrumb">
	    	<li><i class="fa fa-home"></i> Home</li>
	    	<li>Slot</li>
	    	<li class="active">List</li>
	  	</ol>
	</div>
</section>

<!-- Main content -->
<section class="content">
	@include('template.alert')
	
	<div class="box">
	    <div class="box-header with-border">
	      <h3 class="box-title">{{ $title }}</h3>
	    </div>
	    <div class="box-body">
	      <table id="table01" class="table table-bordered table-striped">
	        <thead>
	        <tr>
	          	<th>Slot</th>
	          	<th>Kapasitas</th>
	          	<th>Action</th>
	        </tr>
	        </thead>
	        <tbody>
	        @if (!empty($slot))
		        @foreach ($slot as $key=>$val)
		        	<tr>
			          	<td>{{ $val['slot_name'] }}</td>
			          	<td style="width: 300px;">{{ $val['capacity'] }}</td>
			          	<td style="width: 300px;">
			          		<a href="{{ url('slot/update', $val['id_slot']) }}/{{ str_slug($val['slot_name'], '_') }}" class="btn btn-success"><i class="fa fa-edit"></i> Update </a>

			          		<button class="btn btn-danger delete" data-toggle="confirmation-popout" data-popout="true" data-id="{{ $val['id_slot'] }}"><i class="fa fa-trash"></i> Delete</button>
			          		<form id="delete-{{ $val['id_slot'] }}" action="{{ url('slot/hapus') }}" method="POST" style="display: none;">
			          		    {{ csrf_field() }}
			          		    <input type="hidden" name="id_slot" value="{{ $val['id_slot'] }}">
			          		</form>
			          	</td>
			        </tr>
			    @endforeach
		    @endif
	        </tbody>
	      </table>
	    </div>
	</div>

</section>
<!-- /.content -->


@endsection

@section('script')
<script src="{{ url('bower_components/bootstrap-confirmation/bootstrap-confirmation.min.js') }}"></script>
<script src="{{ url('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>
<script src="{{ url('js/bootstrap-confirmation.js') }}"></script>
<script>
  $(function () {
    $('#table01').DataTable();
    
  });
</script>
<script type="text/javascript">
	$('[data-toggle=confirmation-popout]').confirmation({
	    rootSelector: '[data-toggle=confirmation-popout]',
	    container: 'body'
	});
</script>
<script type="text/javascript">
	$('#table01').on('click', '.delete', function() {
		$('#delete-'+$(this).data('id')).submit();
	});

</script>

@endsection 