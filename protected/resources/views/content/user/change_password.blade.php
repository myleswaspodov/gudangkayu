@extends('template.body')

@section('style')
	<link rel="stylesheet" href="{{ url('bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')		 
<!-- Content Header (Page header) -->
<section class="content-header">
  	<div>
	    <ol class="breadcrumb">
	    	<li><i class="fa fa-home"></i> Home</li>
	    	<li>User</li>
	    	<li class="active">Change Password</li>
	  	</ol>
	</div>
</section>

<!-- Main content -->
<section class="content">
	@include('template.alert')
	
	<div class="box">
	    <div class="box-header with-border">
	      <h3 class="box-title">{{ $title }}</h3>
	    </div>
	    <!-- /.box-header -->
	    <!-- form start -->
	    <form class="form-horizontal" action="{{ url()->current() }}" method="POST">
	      	<div class="box-body">
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Password Sekarang <small style="color: red">*</small> </label> 
		          	<div class="col-md-10">
		            	<input type="password" class="form-control" name="password_now" placeholder="Password sekarang" required maxlength="100" value="{{ old('password_now') }}">
		          	</div>
		        </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Password Baru <small style="color: red">*</small> </label>
		          	<div class="col-md-10">
		            	<input type="password" class="form-control" name="password" placeholder="Password baru" required maxlength="100" >
		          	</div>
		        </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Konfirmasi Password Baru <small style="color: red">*</small> </label></label>
		          	<div class="col-md-10">
		            	<input type="password" class="form-control" name="password_confirmation" placeholder="Konfirmasi password baru" maxlength="100" >
		          	</div>
		        </div>
	      	</div>
	      	
	      <!-- /.box-body -->
	    <div class="box-footer">
			<div class="col-md-2">		
			</div>
			<div class="col-md-10">
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-info">Submit</button>
				{{ csrf_field() }}	
			</div>
	    </div>
	      <!-- /.box-footer -->
	    </form>
	</div>

</section>
<!-- /.content -->


@endsection

@section('script')
<script src="{{ url('bower_components/select2/dist/js/select2.full.min.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('.select2').select2();
	});
</script>
@endsection 